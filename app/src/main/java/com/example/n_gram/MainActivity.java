package com.example.n_gram;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.n_gram.adapter.NgramPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private NgramPagerAdapter ngramPagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ngramPagerAdapter = new NgramPagerAdapter(getSupportFragmentManager());
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);

        viewPager.setAdapter(ngramPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
}
