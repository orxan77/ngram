package com.example.n_gram.network;

import com.example.n_gram.model.Ngram;

public class JSONResponse {

    private Ngram[] content;

    public Ngram[] getContent() {
        return content;
    }
}
