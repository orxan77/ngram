package com.example.n_gram.network;

import com.example.n_gram.model.Ngram;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NgramAPI {

    @GET("gram")
    Call<JSONResponse> getNgrams(@Query("n") int ngramNumber, @Query("size") int size, @Query("page") int page);


}
