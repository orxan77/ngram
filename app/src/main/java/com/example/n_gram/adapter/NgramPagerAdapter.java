package com.example.n_gram.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.n_gram.ui.AdvancedFragment;
import com.example.n_gram.ui.BasicFragment;

public class NgramPagerAdapter extends FragmentPagerAdapter {


    public NgramPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new BasicFragment();
            case 1: return new AdvancedFragment();
        }
        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return "Basic";
            case 1: return "Advanced";
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
