package com.example.n_gram.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.n_gram.R;
import com.example.n_gram.model.Ngram;

import java.util.ArrayList;

public class NgramAdapter extends RecyclerView.Adapter<NgramAdapter.NgramViewHolder> {

    private Context mContext;
    private ArrayList<Ngram> mNgramArrayList;

    public NgramAdapter(Context context, ArrayList<Ngram> ngramArrayList) {
        mContext = context;
        mNgramArrayList = ngramArrayList;
    }

    @NonNull
    @Override
    public NgramViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.ngram_list_item, viewGroup,
                false);
        return new NgramViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NgramViewHolder ngramViewHolder, int i) {
        Ngram currentNgram = mNgramArrayList.get(i);

        ngramViewHolder.mTextViewNgramWord.setText(currentNgram.getNgram1Word());
        if (currentNgram.getNgram2Word() != null)
            ngramViewHolder.mTextViewNgramWord.append("\n" + currentNgram.getNgram2Word());

        if (currentNgram.getNgram3Word() != null)
            ngramViewHolder.mTextViewNgramWord.append("\n" + currentNgram.getNgram3Word());

        if (currentNgram.getNgram4Word() != null)
            ngramViewHolder.mTextViewNgramWord.append("\n" + currentNgram.getNgram4Word());

        if (currentNgram.getNgram5Word() != null)
            ngramViewHolder.mTextViewNgramWord.append("\n" + currentNgram.getNgram5Word());

        ngramViewHolder.mTextViewNgramCount.setText(String.valueOf(currentNgram.getCount()));
    }

    @Override
    public int getItemCount() {
        return mNgramArrayList.size();
    }

    class NgramViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextViewNgramWord;
        private TextView mTextViewNgramCount;

        NgramViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextViewNgramWord = itemView.findViewById(R.id.text_view_ngram_word);
            mTextViewNgramCount = itemView.findViewById(R.id.text_view_ngram_count);
        }
    }
}









