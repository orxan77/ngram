package com.example.n_gram.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.n_gram.NgramListActivity;
import com.example.n_gram.R;

public class BasicFragment extends Fragment implements View.OnClickListener {

    private Button buttons[] = new Button[5];
    private int[] button_ids = {R.id.button_1, R.id.button_2, R.id.button_3, R.id.button_4, R.id.button_5};
    private Button button_unfocus;
    private Button button_go;
    private int selected_number_ngram = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_basic, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button_go = view.findViewById(R.id.button_go_basic);
        button_go.setOnClickListener(this);

        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = view.findViewById(button_ids[i]);
            buttons[i].setBackgroundColor(getResources().getColor(R.color.colorUnselectedBackgroundButton));
            buttons[i].setOnClickListener(this);
        }
        button_unfocus = buttons[0];
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_1:
                setFocus(button_unfocus, buttons[0]);
                selected_number_ngram = 1;
                break;
            case R.id.button_2:
                setFocus(button_unfocus, buttons[1]);
                selected_number_ngram = 2;
                break;
            case R.id.button_3:
                setFocus(button_unfocus, buttons[2]);
                selected_number_ngram = 3;
                break;
            case R.id.button_4:
                setFocus(button_unfocus, buttons[3]);
                selected_number_ngram = 4;
                break;
            case R.id.button_5:
                setFocus(button_unfocus, buttons[4]);
                selected_number_ngram = 5;
                break;
            case R.id.button_go_basic:
                navigateToNgramListActivity();
                break;
        }

    }

    private void setFocus(Button button_unfocus, Button button_focus) {
        button_unfocus.setTextColor(getResources().getColor(R.color.colorTextUnselectedButton));
        button_unfocus.setBackgroundColor(getResources().getColor(R.color.colorUnselectedBackgroundButton));
        button_focus.setTextColor(Color.rgb(255, 255, 255)); // white
        button_focus.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        this.button_unfocus = button_focus;
    }

    private void navigateToNgramListActivity() {
        Intent intent = new Intent(getContext(), NgramListActivity.class);
        intent.putExtra("selected_ngram", selected_number_ngram);
        intent.putExtra("ngram_type", "Basic");
        startActivity(intent);
    }
}
