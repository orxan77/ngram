package com.example.n_gram;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.n_gram.adapter.NgramAdapter;
import com.example.n_gram.model.Ngram;
import com.example.n_gram.network.JSONResponse;
import com.example.n_gram.network.NetworkUtils;
import com.example.n_gram.network.NgramAPI;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NgramListActivity extends AppCompatActivity {

    private static final String TAG = NgramListActivity.class.getSimpleName();
    private NgramAdapter mNgramAdapter;
    private RecyclerView mRecyclerView;
    private Retrofit mRetrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngram_list);

        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        int ngramNumber = extractFromIntent();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(NetworkUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        makeNetworkCall(ngramNumber, 30, 0);

    }

    private void makeNetworkCall(int ngramNumber, int size, int page) {


        final NgramAPI ngramAPI = mRetrofit.create(NgramAPI.class);

        Call<JSONResponse> call = ngramAPI.getNgrams(ngramNumber, size, page);

        call.enqueue(new Callback<JSONResponse>() {

            @SuppressWarnings("NullableProblems")
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonArray = response.body();

                ArrayList<Ngram> ngrams;
                if (jsonArray != null) {
                    ngrams = new ArrayList<>(Arrays.asList(jsonArray.getContent()));

                    Log.e(TAG, "onResponse: " + ngrams.get(0).getNgram1Word() + "  " + ngrams.get(0).getCount());
                } else
                    throw new NullPointerException("JSONArray is null");

                mNgramAdapter = new NgramAdapter(NgramListActivity.this, ngrams);
                mRecyclerView.setAdapter(mNgramAdapter);
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: FAILED FAILED");
                Toast.makeText(NgramListActivity.this, t.getMessage() + "\nError Occurred", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private int extractFromIntent() {
        Intent intent = getIntent();
        return intent.getIntExtra("selected_ngram", 1);
    }


}
