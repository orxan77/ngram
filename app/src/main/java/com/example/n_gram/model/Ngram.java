package com.example.n_gram.model;

import com.google.gson.annotations.SerializedName;

public class Ngram {


    @SerializedName("firstword")
    private String ngram1Word;

    @SerializedName("secondword")
    private String ngram2Word;

    @SerializedName("thirdword")
    private String ngram3Word;

    @SerializedName("fourthword")
    private String ngram4Word;

    @SerializedName("fifthword")
    private String ngram5Word;

    @SerializedName("count")
    private int count;

    public Ngram(String ngram1Word, int count) {
        this.ngram1Word = ngram1Word;
        this.count = count;
    }

    public String getNgram1Word() {
        return ngram1Word;
    }

    public void setNgram1Word(String ngram1Word) {
        this.ngram1Word = ngram1Word;
    }

    public String getNgram2Word() {
        return ngram2Word;
    }

    public void setNgram2Word(String ngram2Word) {
        this.ngram2Word = ngram2Word;
    }

    public String getNgram3Word() {
        return ngram3Word;
    }

    public void setNgram3Word(String ngram3Word) {
        this.ngram3Word = ngram3Word;
    }

    public String getNgram4Word() {
        return ngram4Word;
    }

    public void setNgram4Word(String ngram4Word) {
        this.ngram4Word = ngram4Word;
    }

    public String getNgram5Word() {
        return ngram5Word;
    }

    public void setNgram5Word(String ngram5Word) {
        this.ngram5Word = ngram5Word;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
